class Utils{
    createSuccessResponse(message, resdata, statuscode){
        
        let result = {
            message: message,
            resdata: resdata,
            statuscode: statuscode    
        }
        return result;
    }
}

module.exports = new Utils();