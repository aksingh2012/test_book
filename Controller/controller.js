var express = require('express');
var app = express();
var Book = require('../Model/Book.model');

var bookController = {
    showall : function(){                                                    // Showing all the books promise function
        return new Promise((resolve, reject)=>{
            Book.find({}, function(error, result){
                if(error){
                    return reject(error)
                }
                resolve(result);
                console.log(result);
            });
        });
    },

    showOne : function(req){ 
        return new Promise((resolve, reject) => {
            Book.findOne({_id: req.params.id}, function (error, result) {    // Showing one the books promise function
                if (error) {
                    return reject(error);
                    console.log(req);
                }
                resolve(result);
                console.log(req);
            });
        });
    },
    
    showCreateOne : function(req){
        return new Promise((resolve, reject) => {
            Book.create(req.body, function (error, result) {    // Showing one the books promise function
                if (error) {
                    return reject(error);
                }
                resolve(result)
            });
        });
    },

 

    updataeOne: function(req) {
        return new Promise((resolve, reject) => {
            Book.findOneAndUpdate({_id: req.params.id}, {$set:{title:req.body.title}}, {upset:true},
                function (error, result) {    // Showing updating one books promise function
                if (error) {
                    return reject(error)
                }
                resolve(result)
            });
        });
    },


    removeOne : function(req){
        return new Promise((resolve, reject) => {
            Book.findOneAndRemove({_id: req.params.id}, function (error, result) {    // Showing deleting one  books promise function
                if (error) {
                    return reject(error)
                }
                resolve (result)
            });
        });
    }


};


module.exports = bookController;