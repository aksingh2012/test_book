var express = require('express');
var router = express.Router();
var bookcntr = require('../Controller/controller');
var utils = require('../utils/utility');
//var Book = require('./Book.model');

let result;
class BookController{
    constructor(){}
      
    async getAll(req, res){
       // let result;
        try{
            result = await bookcntr.showall();
            res.send(utils.createSuccessResponse("Get All Book", result, '1001'))
        }catch(err){
            res.status(412).send({message: err, status: false})
        }
    }

    async getOne(req, res){
        // let result;
        try{
            result = await bookcntr.showOne(req);
            res.send(utils.createSuccessResponse("Get one Book", result, '1002'))
        }catch(err){
            res.status(413).send({message: err, status: false})
        }
    }

    async createOne(req, res){
        // let result;
        try{
            result = await bookcntr.showCreateOne(req);
            res.send(utils.createSuccessResponse("create one Book", result, '1003'))
        }catch(err){
            res.status(414).send({message: err, status: false})
        }
    }

    async updateOne(req, res){
        // let result;
        try{
            result = await bookcntr.updataeOne(req);
            res.send(utils.createSuccessResponse("update one Book", result, '1004'))
        }catch(err){
            res.status(415).send({message: err, status: false})
        }
    }

    async deleteOne(req, res){
        // let result;
        try{
            result = await bookcntr.removeOne(req);
            res.send(utils.createSuccessResponse("delete one Book", result, '1005'))
        }catch(err){
            res.status(416).send({message: err, status: false})
        }
    }
}

var book = new BookController();



router.get('/books', book.getAll.bind(book));

router.get('/books/:id',book.getOne.bind(book));

router.post('/books',book.createOne.bind(book));

router.put('/bookrouters/:id',book.updateOne.bind(book));

router.delete('/books/:id',book.deleteOne.bind(book));

module.exports = router;


/********************************************************************************************/






































//router.get('/books/:id',Book.getOne.bind(Book));

//router.post('/books',Book.getAll.bind(Book));

//router.put('/bookrouters/:id',Book.getAll.bind(Book));

//router.delete('/book/:id',Book.getAll.bind(Book));

//module.exports = router;






