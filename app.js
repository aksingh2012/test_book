var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose'); 
var port =8080;  // port assigned 8080 
var routes = require('./Router/Book.route');
var Book = require('./Model/Book.model');

// db connectivity

var db ='mongodb://localhost/sample';
mongoose.connect(db);

// getting  the parameter use bodyParser

app.use(bodyParser.json())  // tells system that you want the json to be used 
app.use(bodyParser.urlencoded({
    extended:true      //  we can write false also where it will say system to use simple algo for parsing or use complex parsing :true
}));


app.use (routes);   


// checking the port connectivity
app.listen(port,function(){
    console.log("port listening at :"+port);
})